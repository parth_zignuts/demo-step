@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
        @endif
       
        <div class="col-md-8 col-md-offset-2" >
            <div class="panel panel-default">
                <div class="panel-body">
                    <p id="step"></p>
                </div>
            </div>
        </div>
    </div>
</div>

 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" >
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body" >
        <img src="{{ asset('public/Happy Holi-03.jpg') }}" width="560" height="400">
         
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>

@endsection

@section('script')

<script type="text/javascript">
     var step=1;
     showDetails(step);
     var perivious = [];
      var name ;
 
  // $(document).on('click','.ans,.next',function(){

  //    var step = $(this).data('next');
  //    var cstep = $('.stepno').data('stepno');
  //    perivious.push(cstep);

  //    showDetails(step);

  // });

  
   $(document).on('click','.ans,.next',function(){

     var s = $(this).data('next');
      
      if (Object.prototype.toString.call(s) === "[object String]") 
     {
      
       var step = s.split(" ");
     }
     else
     {
         
           var step = s;
     }
    

      // if ($('.full_name').val() != '') 
      // {
      //   var name=$('.full_name').val();
       
            // if (step == 2) 
            // {
            //   $('#myModal').modal('show');
            // }
          
           nextStepArray();
           showDetails(step);

      
      // }
      // else
      // {
      //   $('.error').html('Enter your full name').css("color", "red");
      // }
  });


  
  $(document).on('click','.pre',function(){
    
     //var step=$(this).data('pre');
     var step=perivious.slice(-1)[0];
     perivious.pop();
     console.log('previous step  '+step);
     console.log('previous array '+perivious);
     showDetails(step);

  });

 
  $(document).on('click','.mulAnsnext',function(){

     var mulAns =$('.mulAns').val();
        
       //  alert(mulAns);
       // console.log(mulAns);
       nextStepArray();
       
     showDetails(mulAns);

  });

  function showDetails(step){
       
        $.get("{{ route('showDetails') }}",{step:step},function(data){
          
           $('#step').empty().append(data);
       });

        
        
  }

  function nextStepArray() {
     var cstep = $('.stepno').data('stepno');
    
     perivious.push(cstep);
  }


  // $('#foo').css('width','50%')
  
</script>

@endsection