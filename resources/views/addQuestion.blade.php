@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
        @endif

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add Question</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('questionInsert') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="question" class="col-md-4 control-label">Question</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="question"  required autofocus>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="step" class="col-md-4 control-label">Step Number </label>

                            <div class="col-md-6">
                                <input id="step" type="text" class="form-control" name="step" value="{{ $step }}"  readonly autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="next_step" class="col-md-4 control-label">Next Question</label>

                            <div class="col-md-6">
                            
                                <select name="next_step" >
                                <option value="" selected disabled hidden >Choose here</option>
                                @foreach ($questions as $q)
                                   <option value="{{ $q->step }}">({{ $q->step }}) {{ $q->question}}</option>
                                @endforeach
                                
                                </select>
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
   <div class="row">
      <table class="table">
        <tr>
            <th>Id</th>
            <th>Step Number</th>
            <th>Question</th>
            <th>next Step</th>
            <th>edit</th>
        </tr>
         @foreach ($data as $d)
            <tr>
                <td>{{ $d->id }}</td>
                <td>{{ $d->step }}</td>
                <td>{{ $d->question }}</td>
                <td>{{ $d->next_step }}</td>
                <td><a href="{{ route('editQuestion',['id' => $d->id])}}">edit</a></td>
            </tr>
         @endforeach   
      </table>
   </div>
</div>
@endsection
