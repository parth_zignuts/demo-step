@extends('layouts.app')
@section('style')
<style type="text/css">
  #regiration_form fieldset:not(:first-of-type) {
    display: none;
  }
  .demo {
    font-size: 150%;
    color: red;
}
  </style>
@endsection 
@section('content')
<div class="container">
    <div class="row">

<!-- <p class="p" data-slide="0">Rocky</p>
<p class="p" data-slide="1">Amit</p>
<p class="p" data-slide="2">John</p> -->

        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
        @endif
       
        <div class="col-md-8 col-md-offset-2" >
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <form id="regiration_form" novalidate action="action.php"  method="post">
                      <fieldset data-id="1">
                        <h2>Step 1: Create your account</h2>
                        <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Password">
                        </div>
                        <input type="button" name="password" class="next btn btn-info" data-next="3"  value="Next" />
                      </fieldset>
                      <fieldset data-id="2">
                        <h2> Step 2: Add Personnel Details</h2>
                        <div class="form-group">
                        <label for="fName">First Name</label>
                        <input type="text" class="form-control" name="fName" id="fName" placeholder="First Name">
                        </div>
                        <div class="form-group">
                        <label for="lName">Last Name</label>
                        <input type="text" class="form-control" name="lName" id="lName" placeholder="Last Name">
                        </div>
                        <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
                        <input type="button" name="next" class="next btn btn-info" value="Next" />
                      </fieldset>
                      <fieldset data-id="3">
                        <h2> Step 5: Add Personnel Details</h2>
                        <div class="form-group">
                        <label for="fName">First Name</label>
                        <input type="text" class="form-control" name="fName" id="fName" placeholder="First Name">
                        </div>
                        <div class="form-group">
                        <label for="lName">Last Name</label>
                        <input type="text" class="form-control" name="lName" id="lName" placeholder="Last Name">
                        </div>
                        <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
                        <input type="button" name="next" class="next btn btn-info" value="Next" />
                      </fieldset>
                      <fieldset data-id="4">
                        <h2>Step 3: Contact Information</h2>
                        <div class="form-group">
                        <label for="mob">Mobile Number</label>
                        <input type="text" class="form-control" id="mob" placeholder="Mobile Number">
                        </div>
                        <div class="form-group">
                        <label for="address">Address</label>
                        <textarea  class="form-control" name="address" placeholder="Communication Address"></textarea>
                        </div>
                        <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
                        <input type="submit" name="submit" class="submit btn btn-success" value="Submit" />
                      </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
  // $(document).ready(function() {
       
  //       $(".p").click(function(){

  //         var d = $(this).data('slide');
  //      $('[data-slide="'+d+'"]').addClass('demo');
  //     });
  //   });

    $(document).ready(function(){
      var current = 1,current_step,next_step,steps;
      steps = $("fieldset").length;

      $(".next").click(function(){

        var d = $(this).data('next');
        $('[data-id="'+d+'"]').show();
        current_step = $(this).parent();
        next_step = $(this).parent().next();
        //next_step.show();
        current_step.hide();
        setProgressBar(++current);
      });
      $(".previous").click(function(){
        current_step = $(this).parent();
        next_step = $(this).parent().prev();
        next_step.show();
        current_step.hide();
        setProgressBar(--current);
      });
      setProgressBar(current);
      // Change progress bar action
      function setProgressBar(curStep){
        var percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        $(".progress-bar")
          .css("width",percent+"%")
          .html(percent+"%");   
      }
    });
  
</script>

@endsection