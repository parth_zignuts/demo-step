@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
        @endif

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Select Answer for Question</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('answerUpdate') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$answers->id}}">
                        <div class="form-group">
                            <label for="question" class="col-md-4 control-label">Question</label>

                            <div class="col-md-6">
                                <select name="que_id">
                                <option value="" selected disabled hidden>Choose here</option>
                                @foreach ($questions as $q)
                                   <option value="{{ $q->id }}" @if($q->id == $answers->que_id ) selected @endif >{{ $q->question }}</option>
                                @endforeach
                                
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="answer" class="col-md-4 control-label">Answer</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="answer" value="{{ $answers->answer}}" required autofocus>
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="next_step" class="col-md-4 control-label">Next Question</label>

                            <div class="col-md-6">
                                <select name="next_step">
                                <option value="" selected disabled hidden>Choose here</option>
                                @foreach ($questions as $q)
                                   <option value="{{ $q->step }}" @if($q->step == $answers->next_step ) selected @endif>{{ $q->question}}</option>
                                @endforeach
                                
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
