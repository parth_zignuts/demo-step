@extends('layouts.app')
@section('style')
<style type="text/css">
  #all_step fieldset:not(:first-of-type) {
    display: none;
  }
  .demo {
    font-size: 150%;
    color: red;
}
  </style>
@endsection 
@section('content')
<div class="container">
    <div class="row">

<!-- <p class="p" data-slide="0">Rocky</p>
<p class="p" data-slide="1">Amit</p>
<p class="p" data-slide="2">John</p> -->

        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
        @endif
       
        <div class="col-md-8 col-md-offset-2" > 
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="progress" style="height:15px">
                        <div class="progress-bar active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                      
       

                <div id="all_step">
                  @foreach ($questions as $question)
                    <fieldset data-step="{{$question->step}}">
                    step:- {{$question->step }}
                    id :- {{$question->id }}
                      @if(isset($question->image))  
                       <div class="col-md-12" style="text-align: center;">
                           <img src="{{asset('public/images')}}/{{ $question->image }}" width="254">
                       </div>
                      @endif 
   
                       <h3 style="text-align: center;"> {!! $question->question !!} </h3>
                       <br>
                       <p style="text-align: center;">{!! $question->pre_text !!}</p>

                        <div class="ansView" style="text-align: center;">
                          
                            <input type="{{$question->type}}" class="full_name" name="full_name" placeholder="Please enter your full name" style=" text-align: center; width: 342px; border-radius: 20px;" />
                            <br>
                           
                        </div>
                        <br>
                        <p style="text-align: center;" class="as">{!! $question->post_text !!}</p>
                       @if($question->step != 1)
                        <label class="pre" style="float: left;">{{ $question->back_label }}</label>
                       @endif
                      <?php   $a =answerView($question->id); ?>
                      
                       @if($a == 0 || $question->type == "radio" || $question->type == "other")
                          <label class="next" data-next="{{$question->next_step}}" style="float: right;">
                             {{ $question->next_label }}
                          </label>
                       @endif
                       
                    </fieldset>
                  @endforeach
                </div>
                   
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
  

    $(document).ready(function(){
       var perivious = [];
       perivious.push(1);
       var name='';
      var current = 1,current_step,next_step,steps;
      steps = $("fieldset").length;

      
      $(document).on('click','.next',function(){
        
        var cstep = $(this).data('next');

        $('[data-step="'+cstep+'"]').show();
         showDetails(cstep); 
 
        perivious.push(cstep);
        current_step = $(this).parent();
        current_step.hide();
        setProgressBar(++current);
          
          if ($('.full_name').val() != '') 
          {
           var name=$('.full_name').val();
          
           var a=$('.u_name').html(name).css("text-decoration", "underline");
         
          }
        
      });

      $(document).on('click','.ans',function(){
        
        var cstep = $(this).data('next');
        $('[data-step="'+cstep+'"]').show();
         showDetails(cstep); 
          var s=perivious.slice(-1)[0];
          
        perivious.push(cstep);

        $('[data-step="'+s+'"]').hide();
        setProgressBar(++current);
        
      });


      $(document).on('click','.pre',function(){
    
            var step=perivious.slice(-2)[0];
            perivious.pop();
           showDetails(step); 

          $('[data-step="'+step+'"]').show();
          current_step = $(this).parent();
          current_step.hide();
          setProgressBar(--current);
       });


      setProgressBar(current);
      
      function showDetails(step){
       
        $.get("{{ route('showDetails2') }}",{step:step},function(data){
            // console.log(data);
           $('.ansView').empty().append(data);
        });

      }


      // Change progress bar action
      function setProgressBar(curStep){
        var percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        $(".progress-bar")
          .css("width",percent+"%");
          //.html(percent+"%");   
      }

      function userName(){


      }
    });
  
</script>

@endsection