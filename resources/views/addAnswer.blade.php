@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
        @endif

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Select Answer for Question</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('answerInsert') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="question" class="col-md-4 control-label">Question</label>

                            <div class="col-md-6">
                                <select name="que_id" required>
                                <option value="" selected disabled hidden value="">Choose here</option>
                                @foreach ($questions as $q)
                                   <option value="{{ $q->step }}">({{ $q->step }}) {{ $q->question }}</option>
                                @endforeach
                                
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="answer" class="col-md-4 control-label">Answer</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="answer"  required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="next_step" class="col-md-4 control-label">Next Question</label>

                            <div class="col-md-6">
                                <select name="next_step" required>
                                <option value="" selected disabled hidden value="">Choose here</option>
                                @foreach ($questions as $q)
                                   <option value="{{ $q->step }}">({{ $q->step }}) {{ $q->question}}</option>
                                @endforeach
                                
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
   <div class="row">
      <table class="table">
        <tr>
            <th>Id</th>
            <th>Question</th>
            <th>Answer</th>
            <th>Next Question</th>
            <th>edit</th>
        </tr>
         @foreach ($data as $d)
            <tr>
                <td>{{ $d->id }}</td>
                <td>{{ $d->que }}</td>
                <td>{{ $d->answer }}</td>
                <td>({{ $d->next_step }})</td>
                <td><a href="{{ route('editAnswer',['id' => $d->id])}}">edit</a></td>
            </tr>
         @endforeach   
      </table>
   </div>
</div>
@endsection

