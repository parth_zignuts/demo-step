  @if($step == 1)
    <span class="stepno" data-stepno="{{$step}}"/>
        <h3 style="text-align: center;"> {!! $question->question !!} </h3>
         <br>
         <p style="text-align: center;">{!! $question->pre_text !!}</p>
        
          <div class="form-group" style="text-align: center;">
            @if(!isset($answers) || count($answers) == 0)
              <input type="{{$question->type}}" class="full_name" name="full_name" placeholder="Please enter your full name" style=" text-align: center; width: 342px; border-radius: 20px;" /><br>
              <span class="error"></span>
            @else
              
                @foreach ($answers as $a)
                 <label for="question"  class="col-md-4 control-label ans" data-next="{{ $a->next_step}}" >{{$a->answer }}</label> </br></br>

                @endforeach
            @endif 
          </div>
          <p style="text-align: center;">
              <input type="checkbox" name="conf"> {!! $question->post_text !!}
          </p>

          <label class="pre" style="float: left;">{{ $question->back_label }}</label>
          @if(!isset($answers) || count($answers) == 0)
            <label class="next" data-next="{{$question->next_step}}" style="float: right;">
               {{ $question->next_label }}
            </label>
          @endif

  @elseif($step == 2)
    

     <span class="stepno" data-stepno="{{$step}}"/>
        <h3 style="text-align: center;"> {!! $question->question !!} </h3>
         <br>
         <p style="text-align: center;">{!! $question->pre_text !!}</p>

          <div class="form-group" style="text-align: center;">
            @if(!isset($answers) || count($answers) == 0)
              <input type="{{$question->type}}" class="full_name" name="full_name" placeholder="Please enter your Email address" style=" text-align: center; width: 342px; border-radius: 20px;" /><br>
              <span class="error2"></span>
            @else
              
                @foreach ($answers as $a)
                 <label for="question"  class="col-md-4 control-label ans" data-next="{{ $a->next_step}}" >{{$a->answer }}</label> </br></br>

                @endforeach
            @endif 
          </div>
          <p style="text-align: center;" class="as">{!! $question->post_text !!}</p>

          <label class="pre" style="float: left;">{{ $question->back_label }}</label>
          @if(!isset($answers) || count($answers) == 0)
            <label class="next" data-next="{{$question->next_step}}" style="float: right;">
               {{ $question->next_label }}
            </label>
          @endif


 @elseif($step == 3)
    
     <span class="stepno" data-stepno="{{$step}}"/>
        <h3 style="text-align: center;"> {!! $question->question !!} </h3>
         <br>
         <p style="text-align: center;">{!! $question->pre_text !!}</p>

          <div class="form-group" style="text-align: center;">
            @if(isset($answers))
                @foreach ($answers as $a)
                 <div class="col-md-4 col-md-offset-2 ans" data-next="{{ $a->next_step}}" style="text-align: center;">
                    
                     <img src="{{asset('public/images')}}/{{ $a->image }}" width="80">
                    </br>
                    <h4 for="question"  >{{$a->answer }}</h4>
                 </div>
                @endforeach
            @endif 
          </div>
          <p style="text-align: center;" class="as">{!! $question->post_text !!}</p>

          <label class="pre" style="float: left;">{{ $question->back_label }}</label>
          @if(!isset($answers) || count($answers) == 0)
            <label class="next" data-next="{{$question->next_step}}" style="float: right;">
               {{ $question->next_label }}
            </label>
          @endif

  @elseif($step == 4)
    
     <span class="stepno" data-stepno="{{$step}}"/>
       <div class="col-md-12" style="text-align: center;">
          <img src="{{asset('public/images')}}/{{ $question->image }}" width="100">
       </div>
        <h3 style="text-align: center;"> {!! $question->question !!} </h3>
         <br>
         <p style="text-align: center;">{!! $question->pre_text !!}</p>

          <div class="form-group" style="text-align: center;">
            @if(isset($answers))
               
            @endif 
          </div>
          <p style="text-align: center;" class="as">{!! $question->post_text !!}</p>

          <label class="pre" style="float: left;">{{ $question->back_label }}</label>
          @if(!isset($answers) || count($answers) == 0)
            <label class="next" data-next="{{$question->next_step}}" style="float: right;">
               {{ $question->next_label }}
            </label>
          @endif

 @elseif($step == 5)
    
     <span class="stepno" data-stepno="{{$step}}"/>
       <div class="col-md-12" style="text-align: center;">
         <img src="{{asset('public/images')}}/{{ $question->image }}" width="100">
       </div>
        <h3 style="text-align: center;"> {!! $question->question !!} </h3>
         <br>
         <p style="text-align: center;">{!! $question->pre_text !!}</p>

          <div class="form-group" style="text-align: center;">
            @if(isset($answers))
               
            @endif 
          </div>
          <p style="text-align: center;" class="as">{!! $question->post_text !!}</p>

          <label class="pre" style="float: left;">{{ $question->back_label }}</label>
          @if(!isset($answers) || count($answers) == 0)
            <label class="next" data-next="{{$question->next_step}}" style="float: right;">
               {{ $question->next_label }}
            </label>
          @endif

   @elseif($step == 6)
    

     <span class="stepno" data-stepno="{{$step}}"/>
        <h3 style="text-align: center;"> {!! $question->question !!} </h3>
         <br>
         <p style="text-align: center;">{!! $question->pre_text !!}</p>

          <div class="form-group" style="text-align: center;">
            @if(!isset($answers) || count($answers) == 0)
              <input type="{{$question->type}}" class="full_name" name="full_name" placeholder="National Australia Bank" style=" text-align: center; width: 342px; border-radius: 20px;" /><br>
            @endif 
          </div>

          <p style="text-align: center;" class="as">{!! $question->post_text !!}</p>

         <label class="pre" style="float: left;">{{ $question->back_label }}</label>
          @if(!isset($answers) || count($answers) == 0)
            <label class="next" data-next="{{$question->next_step}}" style="float: right;">
               {{ $question->next_label }}
            </label>
          @endif

 @elseif($step == 7)
    
     <span class="stepno" data-stepno="{{$step}}"/>
        <h3 style="text-align: center;"> {!! $question->question !!} </h3>
         <br>
         <p style="text-align: center;">{!! $question->pre_text !!}</p>

          <div class="form-group" style="text-align: center;">
            @if(isset($answers))
                @foreach ($answers as $a)
                 <button type="button" class="col-md-3 ans" data-next="{{ $a->next_step}}" >{{$a->answer }}</button>
                @endforeach
            @endif 
            <br>
          </div>
          <p style="text-align: center;" class="as">{!! $question->post_text !!}</p>

         <label class="pre" style="float: left;">{{ $question->back_label }}</label>
          @if(!isset($answers) || count($answers) == 0)
            <label class="next" data-next="{{$question->next_step}}" style="float: right;">
               {{ $question->next_label }}
            </label>
          @endif


 @else

       
         <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <span class="stepno" data-stepno="{{$step}}"/>
                  <h3> {{ $questions }} </h3>

                </div>

                <div class="panel-body">
                    <div class="form-group">
                    <select class="mulAns" size="6" multiple>
                     @foreach ($answers as $a)
                        <!--  <label for="question"  class="col-md-4 control-label ans" data-next="{{ $a->next_step}}" >{{$a->answer }}</label> </br></br> -->
                        <option value="{{ $a->next_step}}">{{$a->answer }}</option>
                           
                      @endforeach
                      </select>
                    </div>
                </div>

            </div>
            @if($step != 1)
            <h5 class="pre" data-pre="" style="float: left;">Previous</h5>
            @endif
            <h5 class="mulAnsnext" style="float: right;">Next</h5>
          
    </div>   

  @endif 
 