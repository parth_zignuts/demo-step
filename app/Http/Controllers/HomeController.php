<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function addQuestion()
    {
        $data['data']=DB::table('step_que')->get();
        $step=DB::table('step_que')->orderBy('step', 'desc')->value('step'); 
        if ($step != NULL) 
        {
            $data['step']= $step+1;
        }     
        else{
            $data['step']= 1;
        }       
         

         $data['questions']=DB::table('step_que')->orderBy('id', 'asc')->get();
        return view('addQuestion',$data);
    }

    public function questionInsert(Request $request)
    {
        // var_dump($request->all()); exit();
        if ($request->pre_step == NULL) {
            $request->pre_step=1;
            # code...
        }
        $insert=DB::table('step_que')->insert(
                    ['question' => $request->question,'step' => $request->step,'next_step' => $request->next_step]
                );

        if ($insert) {
            return redirect()->back()->with('success', 'IT WORKS!');
        }
        else
        {
            return redirect()->back()->with(['error', 'ERROR !!!!!!']);
        }

    }

    public function editQuestion(Request $request)
    {
       
        $data['questions']=DB::table('step_que')->orderBy('id', 'asc')->get();
        $data['que']=DB::table('step_que')->where('id',$request->id)->first();

       return view('editQuestion',$data);
    }
    
    public function questionUpdate(Request $request)
    {
      
       $update=DB::table('step_que')
            ->where('id',$request->id)
            ->update(['question' => $request->question,'step' => $request->step,'next_step' => $request->next_step]);

        if ($update) {
            return redirect('addQuestion')->with('success', 'Updated!');
        }
        else
        {
            return redirect('addQuestion')->with(['error', 'ERROR !!!!!!']);
        }

    }
    
    public function addAnswer()
    {
        $data['data']=DB::table('step_ans')->join('step_que as sq1', 'sq1.id', '=', 'step_ans.que_id')
                          ->select(['step_ans.*', 'sq1.question as que'])
                      ->get();



        $data['questions']=DB::table('step_que')->orderBy('id', 'asc')->get(); 

        return view('addAnswer',$data);
    }

    public function answerInsert(Request $request)
    {
         //var_dump($request->all()); exit();
        $insert=DB::table('step_ans')->insert(
                    ['que_id' => $request->que_id,
                    'answer' => $request->answer,
                    'next_step' => $request->next_step]
                );

        if ($insert) {
            return redirect()->back()->with('success', 'IT WORKS!');
        }
        else
        {
            return redirect()->back()->with(['error', 'ERROR !!!!!!']);
        }

    }
    
    public function editAnswer(Request $request)
    {
       
        $data['questions']=DB::table('step_que')->orderBy('id', 'asc')->get();
        $data['answers']=DB::table('step_ans')->where('id',$request->id)->first();

       return view('editAnswer',$data);
    }
    
    public function answerUpdate(Request $request)
    {
      
       $update=DB::table('step_ans')
            ->where('id',$request->id)
            ->update(['que_id' => $request->que_id,
                      'answer' => $request->answer,
                      'next_step' => $request->next_step]);
        if ($update) {
            return redirect('addAnswer')->with('success', 'Updated!');
        }
        else
        {
            return redirect('addAnswer')->with(['error', 'ERROR !!!!!!']);
        }

    }
    
    public function showDetails(Request $request)
    {

        if (!is_array($request->step)) {

            $question=DB::table('step_que')->where('step',$request->step)->first();
            $que_id=$question->id;
            $step=$request->step;
            $next=$question->next_step;
            $answers=DB::table('step_ans')->where('que_id',$que_id)->get();
        }
        else
        { 
            $stepArray=$request->step;
            
             $fstep=reset($stepArray);
             
             $question=DB::table('step_que')->where('step',$fstep)->first();
             $step=$fstep;
             $removed = array_shift($stepArray);
             if(!empty($stepArray) ) 
             {
                $next=implode(" ",$stepArray);
             }
             else
             {
                $next=$question->next_step;
             }
                   
        }


        return view('step/stepview2',compact('step','question','next','answers'));
    }

    public function showDetails2(Request $request)
    {
        $question=DB::table('step_que')->where('step',$request->step)->first();
        $que_id=$question->id;
        $step=$request->step;
        $answers=DB::table('step_ans')->where('que_id',$que_id)->get();
       
        return view('step/stepview2',compact('step','question','next','answers'));
    }
 
  

   public function newDetails()
    {
        $data['questions']=DB::table('step_que')->get();
       
        return view('new',$data);
    }

    
}
