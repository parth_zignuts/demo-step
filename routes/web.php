<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Auth::routes();


Route::get('/addQuestion', 'HomeController@addQuestion')->name('addQuestion');
Route::post('/addQuestion/insert', 'HomeController@questionInsert')->name('questionInsert');
Route::get('/editQuestion/{id}', 'HomeController@editQuestion')->name('editQuestion');
Route::post('/addQuestion/update', 'HomeController@questionUpdate')->name('questionUpdate');

Route::get('/addAnswer', 'HomeController@addAnswer')->name('addAnswer');
Route::post('/addAnswer/insert', 'HomeController@answerInsert')->name('answerInsert');
Route::get('/editAnswer/{id}', 'HomeController@editAnswer')->name('editAnswer');
Route::post('/addAnswer/update', 'HomeController@answerUpdate')->name('answerUpdate');

Route::get('/', function () {
    return view('step');
})->name('step');

Route::get('/showDetails', 'HomeController@showDetails')->name('showDetails');
Route::get('/showDetails2', 'HomeController@showDetails2')->name('showDetails2');

Route::get('/new', 'HomeController@newDetails')->name('new');
Route::get('/new2', function () {
    return view('new2');
})->name('new2');